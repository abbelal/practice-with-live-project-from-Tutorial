<?php

include_once '../src/signup/signup.php';
$object = new signup();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $object->prepare($_POST);
    $object->login();
} else {
    $_SESSION['pageError'] = "<h1>404 page not found</h1>";
    header('location:error.php');
}
?>
