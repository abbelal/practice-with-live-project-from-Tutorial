<?php
include_once '../src/signup/signup.php';
$obj = new signup();
$obj->prepare($_GET);
$allData = $obj->dbSingleRowShow();


if (isset($_SESSION['loginedUser']) && !empty($_SESSION['loginedUser'])) {
    ?>
    <!DOCTYPE html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <html lang="en-US" class="no-js"> <!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <meta name="description" content="">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Dashboard</title>
            <link rel="icon" href="image/signup-pen.jpg" />

            <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
            <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
            <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300italic,300,700,600' rel='stylesheet' type='text/css'>

            <link rel="stylesheet" href="css/font-awesome.min.css" /> <!-- for font-awesome -->
            <link rel="stylesheet" href="css/normalize.css">
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <link rel="stylesheet" href="css/html-boilarplate.css">
            <link rel="stylesheet" href="css/animate.min.css">

            <link rel="stylesheet" href="style.css">
            <link rel="stylesheet" href="css/all.forms.style.css">
            <link rel="stylesheet" href="css/responsive.css">


                                                                                                                                <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
              <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->

            <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
        </head>
        <body>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-default">
                            <div class="navbar-header">
                                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a href="#" class="navbar-brand">Project name</a>
                            </div>
                            <div class="navbar-collapse collapse" id="navbar">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="#">Home</a></li>
                                    <li><a href="#">About</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                                <ul class=" focus nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                        <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><?php echo ucfirst($_SESSION['loginedUser']['fullName']); ?> <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="logout.php">Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!--/.nav-collapse -->
                        </nav>
                        <style type="text/css">
                            .table-responsive td{width: 50%;}
                        </style>
                        <p style="font-size: 20px; text-align: center; background: #ccc; width: 50%; margin: 0 auto;"> Profile of : <span style="font-weight: bold;"><?php echo ucfirst($allData['fullName']); ?></span></p>
                        <div class="table-responsive">
                            <table class="table table-bordered" style="width: 50%; margin: 0 auto;"> 
                                <thead> 
                                    <tr> 
                                        <th>Name</th> 
                                        <th>Details</th>  
                                    </tr> 
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td>Full Name</td>
                                        <td><?php echo ucfirst($allData['fullName']); ?></td>
                                    </tr>
                                    <tr>
                                        <td>User Name</td>
                                        <td><?php echo $allData['userName']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Full Name</td>
                                        <td><?php echo $allData['email']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Admin</td>
                                        <td>
                                            <?php
                                            if ($_SESSION['loginedUser']['id'] == $allData['id']) {
                                                echo'Yes';
                                            } else {
                                                echo 'No';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                    if ($_SESSION['loginedUser']['password'] == $allData['password']) {
                                        ?>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <a href="">Update Profile</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <a href="dashboard.php" style="text-align: center; font-weight: bold; font-size: 20px; display: block; margin: 20px auto 0px; padding: 6px; background: #428CC1; color: #fff; width: 150px; border-radius:3px;">Back</a>
                    </div> <!--/end container -->
                </div>
            </div>

            <!-- jQuary library -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
            <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
            <script type="text/javascript" src="js/vendor/modernizr-2.6.2.min.js"></script>
            <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>

            <!-- others jQuary -->
            <script type="text/javascript" src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js/plugins.js"></script>
            <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>

            <!-- all jQuary activation code here and always it will be bottom of all script -->
            <script type="text/javascript" src="js/activation-jQuary.js"></script>
        </body>
    </html>

    <?php
} else {
    $_SESSION['verifyedMsg'] = "You have to Login First";
    header('location:login.php');
}
?>
    