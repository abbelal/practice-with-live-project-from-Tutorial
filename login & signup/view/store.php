<?php

include_once '../src/signup/signup.php';

$object = new signup();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $object->prepare($_POST);
    $data = $object->signupValidation();
    $object->store();
} else {
    $_SESSION['pageError'] = "<h1>404 page not found</h1>";
    header('location:error.php');
}
?>
