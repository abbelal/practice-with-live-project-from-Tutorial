<?php
include_once '../src/signup/signup.php';
$obj = new signup();
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US" class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sign Up</title>
        <link rel="icon" href="image/signup-pen.jpg" />

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300italic,300,700,600' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/font-awesome.min.css" /> <!-- for font-awesome -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/html-boilarplate.css">
        <link rel="stylesheet" href="css/animate.min.css">

        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/all.forms.style.css">
        <link rel="stylesheet" href="css/responsive.css">


        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
    </head>
    <body>


        <section class="account-signUp2"> <!-- start signUp for account section -->
            <div class="signup2-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="#">Project name</a>
                                </div>
                                <div id="navbar" class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav">
                                        <li class="active"><a href="#">Home</a></li>
                                        <li><a href="#">About</a></li>
                                        <li><a href="#">Contact</a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li class="dropdown-header">Nav header</li>
                                                <li><a href="#">Separated link</a></li>
                                                <li><a href="#">One more separated link</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="focus nav navbar-nav navbar-right">
                                        <li class="active"><a href="create.php">Sign Up <span class="sr-only">(current)</span></a></li>
                                        <li><a href="login.php">Login</a></li>
                                    </ul>
                                </div><!--/.nav-collapse -->
                            </div><!--/.container-fluid -->
                        </nav>

                        <div class="signup2-wrapper forMobileDevice">
                            <div class="header">
                                <i class="fa fa-times"></i>
                                <img src="image/signup-pen.jpg" alt="signup-pen" />
                                <h2>Sign Up Now</h2>
                            </div>
                            <p class="text-success" style="font-size: 15px;text-align: center; margin-bottom: 20px;"><?php $obj->errMsg('successMSg'); ?></p>
                            <div class="form-wrapper">
                                <form action="store.php" method="POST">
                                    <div class="form-group">
                                        <input type="text" name="fname" value="<?php if(isset ($_SESSION['formsData']['fname'])){echo $_SESSION['formsData']['fname'];unset ($_SESSION['formsData']['fname']);} ?>" class="form-control fname" id="exampleInputText1" placeholder="First Name">
                                        <input type="text" name="lname" value="<?php if(isset ($_SESSION['formsData']['lname'])){echo $_SESSION['formsData']['lname'];unset ($_SESSION['formsData']['lname']);} ?>" class="form-control lname" id="exampleInputText2" placeholder="Last Name">
                                    </div>

                                    <div class="form-group">
                                        <input type="text" name="userName" value="<?php if(isset ($_SESSION['formsData']['userName'])){echo $_SESSION['formsData']['userName'];unset ($_SESSION['formsData']['userName']);} ?>" class="form-control" id="exampleInputEmail1" placeholder="User Name" />
                                        <p class="text-danger"> <?php $obj->errMsg('errName'); ?> </p>
                                    </div>

                                    <div class="form-group">
                                        <input type="email" name="email" value="<?php if(isset ($_SESSION['formsData']['email'])){echo $_SESSION['formsData']['email'];unset ($_SESSION['formsData']['email']);} ?>" class="form-control" id="exampleInputEmail1" placeholder="Email">
                                        <p class="text-danger"> <?php $obj->errMsg('errEmail'); ?> </p>
                                    </div>

                                    <div class="form-group">
                                        <input type="password" name="password" value="<?php if(isset ($_SESSION['formsData']['password'])){echo $_SESSION['formsData']['password'];unset ($_SESSION['formsData']['password']);} ?>" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                        <p class="text-danger"> <?php $obj->errMsg('errPassword'); ?> </p>
                                    </div>

                                    <div class="form-group">
                                        <input type="password" name="repPassword" value="<?php if(isset ($_SESSION['formsData']['repPassword'])){echo $_SESSION['formsData']['repPassword'];unset ($_SESSION['formsData']['repPassword']);} ?>" class="form-control" id="exampleInputPassword2" placeholder="Confirm Password">
                                        <p class="text-danger"> <?php $obj->errMsg('errRepPassword'); ?> </p>
                                    </div>

                                    <div class="form-group buttonWrapper pull-right">
                                        <a href="../index.php" class="cancel">Back to Home</a>
                                        <button class="submit">submit</button>
                                    </div>
                                    <div class="clear"></div>
                                </form>
                            </div>
                            <footer><p>Already have an account? <a href="login.php"> Login Here</a></p></footer>
                        </div> <!-- end wrapper -->                       
                    </div>
                </div>
            </div>
        </section> <!-- end signUp for account section -->






        <!-- jQuary library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script type="text/javascript" src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>

        <!-- others jQuary -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/jquery.scrollUp.min.js"></script>

        <!-- all jQuary activation code here and always it will be bottom of all script -->
        <script type="text/javascript" src="js/activation-jQuary.js"></script>
    </body>
</html>
